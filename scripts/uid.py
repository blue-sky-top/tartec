# @Time    : 2024/1/22 21:02
# @Author  : 🍁
# @File    : uid.py
# @Software: PyCharm

import uuid
import ulid


def get_uuid():
    """ 生成uuid """
    return str(uuid.uuid4()).replace("-", "")


def get_ulid(s):
    return ulid.from_uuid(s)


if __name__ == '__main__':
    print(get_uuid())
    print(get_ulid(uuid.uuid4()))
