# @Time    : 2024/1/22 18:41
# @Author  : 🍁
# @File    : init__price_policy.py
# @Software: PyCharm
import init__base
from web.models import PriceClassify


def run():
    exists = PriceClassify.objects.filter(classify=1, title="个人免费版").exists()
    price_classify = PriceClassify.objects.create(
        classify=2,  # 默认是给的免费版
        title="VIP版本",  # 标题
        price=500,  # 价格是0
        create_projects=50,  # 创建的项目是5个
        everyone_project_member=10,  # 每个项目参与的人数
        every_project_space=2048,  # 每个项目的大小(M)
        project_file_space=128,  # 单个文件上传的大小(M)
    )


if __name__ == '__main__':
    run()
