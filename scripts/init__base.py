# @Time    : 2024/1/22 18:58
# @Author  : 🍁
# @File    : init__base.py
# @Software: PyCharm
import os
import sys

import django

# os.path.dirname(os.path.abspath(__file__)) 当前文件的文件夹
# os.path.abspath(__file__) 当前文件的路径
base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 当前文件的文件夹的上一级目录
sys.path.append(base_dir)  # 添加到项目目录

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tartec.settings")
django.setup()  # 导入项目当中的配置文件，并运行

