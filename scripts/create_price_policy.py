# @Time    : 2024/3/2 20:51
# @Author  : 🍁
# @File    : create_price_policy.py
# @Software: PyCharm

import os
import sys

import django

# os.path.dirname(os.path.abspath(__file__)) 当前文件的文件夹
# os.path.abspath(__file__) 当前文件的路径
base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 当前文件的文件夹的上一级目录
sys.path.append(base_dir)  # 添加到项目目录

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tartec.settings")
django.setup()  # 导入项目当中的配置文件，并运行

from web import models

def run():
    models.PriceClassify.objects.create(
        title='SVIP',
        price=200,
        create_projects=150,
        everyone_project_member=110,
        every_project_space=110,
        project_file_space=1024,
    )

    models.PriceClassify.objects.create(
        title='SSVIP',
        price=500,
        create_projects=550,
        everyone_project_member=510,
        every_project_space=510,
        project_file_space=2048,
    )


if __name__ == '__main__':
    run()
