# @Time    : 2024/1/21 17:05
# @Author  : 🍁
# @File    : init_user.py
# @Software: PyCharm
import os
import sys

import django

# os.path.dirname(os.path.abspath(__file__)) 当前文件的文件夹
# os.path.abspath(__file__) 当前文件的路径
base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 当前文件的文件夹的上一级目录
sys.path.append(base_dir)  # 添加到项目目录

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tartec.settings")
django.setup()  # 导入项目当中的配置文件，并运行

# 先执行上面的代码，把配置文件添加进去，在执行后面的代码
# 否则会报错
from web import models

# 往数据库当中添加数据：链接数据库，操作，关闭数据库
user = models.User.objects.create(
    username="zhangsan",
    email="234532563@qq.com",
    mobile="13212778599",
    password="123123"
)
