# @Time    : 2024/2/7 20:38
# @Author  : 🍁
# @File    : cos_upload_demo.py
# @Software: PyCharm

from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client
import sys

# SecretId:     AKIDFKZwFQURJIC0c0QuidgvdEthaNCvljbc
#
# SecretKey:    GPUakx20sqy7JkHkGrSzqmgkmr5ATuS0

# 1. 设置用户属性, 包括 secret_id, secret_key, region 等。Appid 已在 CosConfig 中移除，请在参数 Bucket 中带上 Appid。Bucket 由 BucketName-Appid 组成
tmp_secret_id = 'AKIDFKZwFQURJIC0c0QuidgvdEthaNCvljbc'  # 临时密钥的 SecretId，临时密钥生成和使用指引参见 https://cloud.tencent.com/document/product/436/14048
tmp_secret_key = 'GPUakx20sqy7JkHkGrSzqmgkmr5ATuS0'  # 临时密钥的 SecretKey，临时密钥生成和使用指引参见 https://cloud.tencent.com/document/product/436/14048
region = 'ap-beijing'  # 替换为用户的 region，已创建桶归属的 region 可以在控制台查看，https://console.cloud.tencent.com/cos5/bucket

config = CosConfig(Region=region, SecretId=tmp_secret_id, SecretKey=tmp_secret_key)
client = CosS3Client(config)

response = client.create_bucket(
    Bucket='examplebucket-1250000000',
    ACL="public-read"  # private / public-read / public-read-write
)
