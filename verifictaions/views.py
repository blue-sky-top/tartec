from django.shortcuts import render
from django import http
from django.views import View
# Create your views here.
from .rong_lian_yun import ccp_sms
from utils import re_templates
import random, re
from PIL import Image


class SMS_view(View):
    def get(self, request, mobile):
        mobile = request.GET.get("mobile")
        if not re.match(re_templates.MOBILE, mobile):
            return http.HttpResponseForbidden("手机号码不合法")

        sms_msg = random.randint(100000, 999999)
        send_sms = ccp_sms.CCP

        send_sms.send_message(mobile, [sms_msg, "1"], 1)

        return http.JsonResponse({"code": 0, "msg": "发送成功"})


class ImageCode(View):
    def get(self, request, mobile):
        return
