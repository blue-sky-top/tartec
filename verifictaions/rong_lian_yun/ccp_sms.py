# @Time    : 2023/12/12 19:59
# @Author  : 🍁
# @File    : ccp_sms.py
# @Software: PyCharm
import json

from ronglian_sms_sdk import SmsSDK

accId = '2c94811c88bf3503018920413ab01996'
accToken = 'a6ac8a58351f40f19114c9ab0a3d0471'
appId = '2c94811c88bf3503018920413c04199d'


class CCP(object):
    # 使用单例模式
    _instance = None

    def __new__(cls, *args, **kwargs):
        # 给对象分配内存空间
        if cls._instance is None:
            # 判断属性是否为空 ， 为空说明这个类还未生成对象
            # 生成对象 ， 给对象分配内存空间
            cls._instance = super().__new__(cls, *args, **kwargs)
        # new 必须要有返回值 ， 返回的是对象的引用（对象内存地址）
        return cls._instance

    def send_message(self, mobile, datas, tid):
        sdk = SmsSDK(accId, accToken, appId)
        resp = sdk.sendMessage(tid, mobile, datas)
        # print(resp)
        # print(type(resp))
        res = json.loads(resp)
        if res['statusCode'] == '000000':
            return 0
        else:
            return -1


sand_code = CCP()

if __name__ == '__main__':
    c = CCP()
    c.send_message('17841687578', ('1234', '1'), 1)
