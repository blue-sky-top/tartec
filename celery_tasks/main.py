# @Time    : 2023/12/13 12:02
# @Author  : 🍁
# @File    : main.py
# @Software: PyCharm
from celery import Celery

# 启动celery命令
# celery -A celery_tasks.main worker -l info -P eventlet

# 为celery使用django配置文件进行设置
import os

# celery 启动命令
# python3 -m celery -A celery_tasks.main worker -l info -P eventlet

# 更新settings路径
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'tartec.settings'

# 创建celery实例对象
celery_app = Celery('tartec')

# 加载配置
celery_app.config_from_object('celery_tasks.config')

# 自动注册任务
celery_app.autodiscover_tasks(['celery_tasks.sms', "celery_tasks.image_code"])
