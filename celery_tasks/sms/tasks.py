# @Time    : 2023/12/13 12:02
# @Author  : 🍁
# @File    : tasks.py
# @Software: PyCharm

from celery_tasks.main import celery_app
from celery_tasks.sms.rong_lian_yun.ccp_sms import CCP
from . import constants


@celery_app.task(name="sms_tasks")
def send_sms(mobile, code):
    sms_code = CCP().send_message(mobile, [code, constants.SMS_CODE_REDIS_EXPIRES // 60], constants.SEND_SMS_TEMPLATE_ID)
    return sms_code
