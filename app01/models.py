from django.db import models


# Create your models here.

class UserTest(models.Model):
    """
    用户信息表
    """
    username = models.CharField(max_length=20, verbose_name='用户名')
    password = models.CharField(max_length=20, verbose_name='密码')
    email = models.EmailField(max_length=50, verbose_name='邮箱')
    mobile = models.CharField(max_length=11, verbose_name='手机号')

    class Meta:
        db_table = 'user_test'
        verbose_name = '用户信息'
        verbose_name_plural = verbose_name
