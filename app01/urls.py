# @Time    : 2023/12/13 13:08
# @Author  : 🍁
# @File    : urls.py
# @Software: PyCharm

from django.urls import path, re_path
from . import views

urlpatterns = [
    path('register/', views.RegisterView.as_view(), name='register'),
    re_path(r"register/(?P<mobile>1[3-9]\d{9})/", views.Send_SMS_view.as_view(), name="register"),
    path("cos/credential/", views.CosCredential.as_view()),
    path("cos/", views.UploadView.as_view(), name="cos"),
]
