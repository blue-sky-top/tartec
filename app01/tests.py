from django.test import TestCase


# Create your tests here.

class A():
    def __init__(self):
        self.m = None

    def b(self):
        self.m = 1

    def c(self):
        print(self.m)


if __name__ == '__main__':
    A().b()
    A().c()
