from django.test import TestCase

# from django.conf import settings
# import bcrypt
#
#
# # 加密密码
# def hash_password(password):
#     # 生成盐值
#     salt = bcrypt.gensalt(rounds=12)  # rounds参数可以根据需要调整计算强度，默认是12
#     # salt = settings.SECRET_KEY.encode("utf-8")
#     hashed_password = bcrypt.hashpw(password.encode('utf-8'), salt)
#
#     return hashed_password
#
#
# # 将hashed_password存储到数据库等安全位置
#
# # 验证密码
# def check_password(password, hashed_password_from_db):
#     if bcrypt.checkpw(password.encode('utf-8'), hashed_password_from_db):
#         return True
#         # 可以继续执行登录逻辑
#     else:
#         return False
#
#
# # Create your tests here.
#
# if __name__ == '__main__':
#     # 假设用户输入的密码
#     password = "123qwe"
#
#     # 对密码进行加密
#     hashed_password = hash_password(password)
#     print(hashed_password)
#     password = "123qwe"
#     hashed_password = hash_password(password)
#     print(hashed_password)
#
#     # 当用户登录时，从数据库获取已存储的哈希密码并验证
#     stored_hashed_password = hashed_password  # 从数据库或其他存储介质获取哈希过的密码
#     if not check_password(password, stored_hashed_password):
#         print(1)
#     else:
#         print(2)
