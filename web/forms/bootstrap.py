# @Time    : 2024/1/17 20:10
# @Author  : 🍁
# @File    : bootstrap.py
# @Software: PyCharm

class BootStrapForm():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # 给所有字段添加样式
        for field in iter(self.fields):
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].widget.attrs['placeholder'] = '请输入' + self.fields[field].label
            self.fields[field].widget.attrs['v-model'] = field
            self.fields[field].widget.attrs['@blur'] = "check_" + field
            if self.fields[field] == 'uuid':
                continue


class BootStrap():
    # 字段去掉BootStrap 样式
    BootStrap_class_exclude = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if name in self.BootStrap_class_exclude:
                continue
            old_class = field.widget.attrs.get('class', "")  # 拿到forms 里面写的class，没有就是空字符串
            field.widget.attrs['class'] = '{} form-control'.format(old_class)  # 添加到class
            field.widget.attrs['placeholder'] = '请输入%s' % (field.label,)
