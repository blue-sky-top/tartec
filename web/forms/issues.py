# @Time    : 2024/2/26 18:17
# @Author  : 🍁
# @File    : issues.py
# @Software: PyCharm
from django import forms

from web.forms import bootstrap
from web import models
from web.models import Issues


class IssuesForm(bootstrap.BootStrap, forms.ModelForm):
    class Meta:
        model = Issues
        exclude = ["project", "creator", "create_datetime", "latest_update_datetime"]
        widgets = {
            "assign": forms.Select(attrs=({"class": "selectpicker", "data-live-search": "true"})),
            "attention": forms.SelectMultiple(
                attrs=({"class": "selectpicker", "data-live-search": "true", "data-actions-box": "true"})),
            "parent": forms.Select(attrs=({"class": "selectpicker", "data-live-search": "true"})),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # 处理数据初始化

        # 1.获取当前项目的所有问题类型 [(1,'xx'),(2,"xx")]
        self.fields['issues_type'].choices = models.IssuesType.objects.filter(
            project=request.tartec.project).values_list('id', 'title')

        # 2.获取当前项目的所有模块
        module_list = [("", "没有选中任何项"), ]
        module_object_list = models.Module.objects.filter(project=request.tartec.project).values_list('id', 'title')
        module_list.extend(module_object_list)
        self.fields['module'].choices = module_list

        # 3.指派和关注者
        # 数据库找到当前项目的参与者 和 创建者
        total_user_list = [(request.tartec.project.created_by_id, request.tartec.project.created_by.username), ]
        project_user_list = models.UserProject.objects.filter(project=request.tartec.project).values_list('user_id',
                                                                                                          'user__username')
        total_user_list.extend(project_user_list)

        self.fields['assign'].choices = [("", "没有选中任何项")] + total_user_list
        self.fields['attention'].choices = total_user_list

        # 4. 当前项目已创建的问题
        parent_list = [("", "没有选中任何项")]
        parent_object_list = models.Issues.objects.filter(project=request.tartec.project).values_list('id', 'subject')
        parent_list.extend(parent_object_list)
        self.fields['parent'].choices = parent_list


class IssuesRecordingForm(forms.ModelForm):
    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    class Meta:
        model = models.RecordingModel
        fields = ["action", "parent"]


class InviteForms(bootstrap.BootStrap, forms.ModelForm):
    class Meta:
        model = models.ProjectInvite
        fields = ["period", "count"]
