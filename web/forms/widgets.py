# @Time    : 2024/1/27 21:51
# @Author  : 🍁
# @File    : widgets.py
# @Software: PyCharm

from django.forms.widgets import RadioSelect


class ColorWidgets(RadioSelect):
    # template_name = "django/forms/widgets/radio.html"
    # option_template_name = "django/forms/widgets/radio_option.html"

    template_name = "widgets/color_radio/radio.html"
    option_template_name = "widgets/color_radio/radio_option.html"
