# @Time    : 2024/1/30 18:26
# @Author  : 🍁
# @File    : statistic.py
# @Software: PyCharm

from django import forms
from django.core.exceptions import ValidationError
from web import models
from web.forms.bootstrap import BootStrap


class WikiModelForm(BootStrap, forms.ModelForm):
    class Meta():
        model = models.Wiki
        fields = ["title", "content", "parent_id"]
        labels = {
            "parent_id": "上级目录"
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request
        # 找到想要的字段把它绑定显示的数据重置
        # 找到所有当前项目的标题
        table_data_list = [("", "请选择")]
        data_list = models.Wiki.objects.filter(project_id=request.tartec.project.id).values_list("id", "title")
        table_data_list.extend(data_list)
        self.fields["parent_id"].choices = table_data_list
