from django.db import models


# Create your models here.

# 用户信息表
class User(models.Model):
    """
    用户信息表
    """
    username = models.CharField(max_length=20, verbose_name='用户名', db_index=True)  # db_index=True 在数据库中创建索引，使其查询的更快
    password = models.CharField(max_length=64, verbose_name='密码')
    email = models.EmailField(max_length=50, verbose_name='邮箱')
    mobile = models.CharField(max_length=11, verbose_name='手机号', db_index=True)

    price_classify = models.ForeignKey(verbose_name="价格策略", to="PriceClassify", on_delete=models.CASCADE, null=True,
                                       blank=True, default=1)

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'user'
        verbose_name = '用户信息'
        verbose_name_plural = verbose_name


# 价格分布表
class PriceClassify(models.Model):
    """ 价格分布表 """
    price_classify = (
        (1, "免费版"),
        (2, "收费版"),
        (3, "其他")
    )
    # SmallIntegerField 小的非负整数
    classify = models.SmallIntegerField(verbose_name="收费类型", choices=price_classify, default=1)
    title = models.CharField(max_length=20, verbose_name="标题")
    # PositiveIntegerField 只能存储正整数值
    price = models.PositiveIntegerField(verbose_name="价格", null=True, blank=True, default=None)

    create_projects = models.PositiveIntegerField(verbose_name="创建项目个数")
    everyone_project_member = models.PositiveIntegerField(verbose_name="每个项目成员个数")
    every_project_space = models.BigIntegerField(verbose_name="每个项目空间", help_text="G")
    project_file_space = models.BigIntegerField(verbose_name="单文件空间", help_text="M")

    create_datetime = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)

    class Meta:
        db_table = "price"
        verbose_name = "价格策略"
        verbose_name_plural = verbose_name


# 用户价格方案
class TransactionRecord(models.Model):
    """ 用户价格方案 """
    state_choice = (
        (1, "已支付"),
        (2, "未支付")
    )
    state = models.SmallIntegerField(verbose_name="支付状态", choices=state_choice, default=2)
    order = models.CharField(verbose_name="订单号", max_length=64, unique=True)
    user = models.ForeignKey(verbose_name="用户", to="User", on_delete=models.CASCADE)

    price_classify = models.ForeignKey(verbose_name="价格策略", to="PriceClassify", on_delete=models.CASCADE)
    count = models.IntegerField(verbose_name="数量(年)", help_text="0表示无限期")
    price = models.PositiveIntegerField(verbose_name="实际支付")

    start_date = models.DateTimeField(verbose_name="开始时间", null=True, blank=True)
    end_date = models.DateTimeField(verbose_name="结束时间", null=True, blank=True)
    create_date = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)

    class Meta:
        db_table = "transaction"
        verbose_name = "交易记录"
        verbose_name_plural = verbose_name


# 新建项目
class Project(models.Model):
    """ 新建项目 """
    COLOR_CHOICE = (
        (1, "#0000FF"),
        (2, "#BBFFFF"),
        (3, "#FFF0F5"),
        (4, "#708090"),
        (5, "#FF69B4"),
        (6, "#436EEE"),
        (7, "#9C9C9C"),
    )
    project_title = models.CharField(verbose_name="项目名称", max_length=32)
    color = models.SmallIntegerField(verbose_name="颜色", choices=COLOR_CHOICE, default=1)
    description = models.CharField(verbose_name="描述", max_length=255, null=True, blank=True)
    star = models.BooleanField(verbose_name="星标", default=False)
    space_used = models.BigIntegerField(verbose_name="已使用空间", default=0, help_text="字节")

    participant = models.PositiveIntegerField(verbose_name="参与人数", default=1)
    created_by = models.ForeignKey(verbose_name="创建者", to="User", on_delete=models.CASCADE)
    create_date = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)

    bucket = models.CharField(verbose_name="COS桶", max_length=128, null=True, blank=True)
    region = models.CharField(verbose_name="COS桶区域", max_length=32, null=True, blank=True)

    class Meta:
        db_table = "project"
        verbose_name = "项目"
        verbose_name_plural = verbose_name


# 项目参与者
class UserProject(models.Model):
    """ 项目参与者 """
    user = models.ForeignKey(verbose_name="参与者", to="User", related_name="projects", on_delete=models.CASCADE)
    project = models.ForeignKey(verbose_name="项目", to="Project", on_delete=models.CASCADE)
    star = models.BooleanField(verbose_name="星标", default=False)
    create_date = models.DateTimeField(verbose_name="加入时间", auto_now_add=True)

    class Meta:
        db_table = "user_project"
        verbose_name = "项目参与者"
        verbose_name_plural = verbose_name


# wiki 管理
class Wiki(models.Model):
    """ wiki """
    project_id = models.ForeignKey(verbose_name="项目", to="Project", on_delete=models.CASCADE)
    title = models.CharField(max_length=20, verbose_name="标题")
    content = models.TextField(verbose_name="内容")
    depth = models.SmallIntegerField(verbose_name="标签深度", default=1)

    # related_name="children" 设置反向查询的名字
    parent_id = models.ForeignKey(verbose_name="父ID", to="Wiki", on_delete=models.CASCADE, related_name="children",
                                  default=None, blank=True,
                                  null=True)

    def __str__(self):
        return self.title  # 返回标题作为字符串

    class Meta():
        db_table = "wiki"
        verbose_name = "wiki知识库"
        verbose_name_plural = verbose_name


# 文件
class File(models.Model):
    file_types = (
        (1, "文件"),
        (2, "文件夹")
    )
    project_id = models.ForeignKey(to="Project", on_delete=models.CASCADE, verbose_name="项目ID")
    title = models.CharField(max_length=25, verbose_name="文件名称")
    type = models.SmallIntegerField(verbose_name="文件类型", choices=file_types, default=1)
    size = models.CharField(max_length=64, verbose_name="文件大小", help_text="字节", blank=True, null=True,
                            default=None)
    key = models.CharField(max_length=128, verbose_name="文件key", blank=True, null=True, default=None)
    parent_id = models.ForeignKey(to="self", on_delete=models.CASCADE, related_name="children", verbose_name="父文件",
                                  default=None, blank=True, null=True)
    file_path = models.CharField(max_length=255, verbose_name="文件路径", blank=True, null=True)

    revise = models.ForeignKey(to="User", on_delete=models.SET("-"), related_name="user_id", verbose_name="修改者")
    create_date = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_date = models.DateTimeField(verbose_name="修改时间", auto_now=True)

    class Meta():
        db_table = "file"
        verbose_name = "文件管理"
        verbose_name_plural = verbose_name


# 问题
class Issues(models.Model):
    """ 问题 """
    project = models.ForeignKey(verbose_name='项目', to='Project', on_delete=models.CASCADE)
    issues_type = models.ForeignKey(verbose_name='问题类型', to='IssuesType', on_delete=models.CASCADE)
    module = models.ForeignKey(verbose_name='模块', to='Module', on_delete=models.CASCADE, null=True, blank=True)

    subject = models.CharField(verbose_name='主题', max_length=80)
    desc = models.TextField(verbose_name='问题描述')
    priority_choices = (
        ("danger", "高"),
        ("warning", "中"),
        ("success", "低"),
    )
    priority = models.CharField(verbose_name='优先级', max_length=12, choices=priority_choices, default='danger')

    # 新建、处理中、已解决、已忽略、待反馈、已关闭、重新打开
    status_choices = (
        (1, '新建'),
        (2, '处理中'),
        (3, '已解决'),
        (4, '已忽略'),
        (5, '待反馈'),
        (6, '已关闭'),
        (7, '重新打开'),
    )
    status = models.SmallIntegerField(verbose_name='状态', choices=status_choices, default=1)

    assign = models.ForeignKey(verbose_name='指派', to='User', on_delete=models.CASCADE, related_name='task', null=True,
                               blank=True)
    attention = models.ManyToManyField(verbose_name='关注者', to='User',
                                       related_name='observe', blank=True)

    start_date = models.DateField(verbose_name='开始时间', null=True, blank=True)
    end_date = models.DateField(verbose_name='结束时间', null=True, blank=True)
    mode_choices = (
        (1, '公开模式'),
        (2, '隐私模式'),
    )
    mode = models.SmallIntegerField(verbose_name='模式', choices=mode_choices, default=1)

    parent = models.ForeignKey(verbose_name='父问题', to='self', related_name='child', null=True, blank=True,
                               on_delete=models.SET_NULL)

    creator = models.ForeignKey(verbose_name='创建者', to='User', on_delete=models.CASCADE,
                                related_name='create_problems')

    create_datetime = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    latest_update_datetime = models.DateTimeField(verbose_name='最后更新时间', auto_now=True)

    def __str__(self):
        return self.subject

    class Meta():
        db_table = "issues"
        verbose_name = "问题"
        verbose_name_plural = verbose_name


class Module(models.Model):
    """ 模块（里程碑）"""
    project = models.ForeignKey(verbose_name='项目', to='Project', on_delete=models.CASCADE)
    title = models.CharField(verbose_name='模块名称', max_length=32)

    def __str__(self):
        return self.title

    class Meta():
        db_table = "issues_model"
        verbose_name = "问题模块"
        verbose_name_plural = verbose_name


class IssuesType(models.Model):
    """ 问题类型 例如：任务、功能、Bug """

    PROJECT_INIT_LIST = ["任务", '功能', 'Bug']

    title = models.CharField(verbose_name='类型名称', max_length=32)
    project = models.ForeignKey(verbose_name='项目', to='Project', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta():
        db_table = "issues_type"
        verbose_name = "问题类型"
        verbose_name_plural = verbose_name


class RecordingModel(models.Model):
    issues = models.ForeignKey(to="Issues", on_delete=models.CASCADE, verbose_name="问题对象")
    action = models.TextField(verbose_name="操作")
    RECORDING_STATUS = (
        (1, "修改记录"),
        (2, "回复")
    )
    status = models.SmallIntegerField(verbose_name="操作状态", choices=RECORDING_STATUS)
    user = models.ForeignKey(to="User", on_delete=models.CASCADE, verbose_name="修改者")

    parent = models.ForeignKey(to="self", on_delete=models.CASCADE, verbose_name="父级操作", related_name="child",
                               null=True, blank=True)
    create_date = models.DateField(verbose_name="操作时间", auto_now_add=True)

    class Meta():
        db_table = "record"
        verbose_name = "操作记录"
        verbose_name_plural = verbose_name


class ProjectInvite(models.Model):
    """ 项目邀请码 """
    project = models.ForeignKey(verbose_name='项目', to='Project', on_delete=models.CASCADE)
    code = models.CharField(verbose_name='邀请码', max_length=64, unique=True)
    count = models.PositiveIntegerField(verbose_name='限制数量', null=True, blank=True, help_text='空表示无数量限制')
    use_count = models.PositiveIntegerField(verbose_name='已邀请数量', default=0)
    period_choices = (
        (30, '30分钟'),
        (60, '1小时'),
        (300, '5小时'),
        (1440, '24小时'),
    )
    period = models.IntegerField(verbose_name='有效期', choices=period_choices, default=1440)
    create_datetime = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    creator = models.ForeignKey(verbose_name='创建者', to='User', on_delete=models.CASCADE,
                                related_name='create_invite')

    class Meta():
        db_table = "invite"
        verbose_name = "邀请码"
        verbose_name_plural = verbose_name
