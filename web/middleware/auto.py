# @Time    : 2024/1/20 19:13
# @Author  : 🍁
# @File    : auto.py
# @Software: PyCharm
import datetime
import re

from django.shortcuts import redirect
from django.utils.deprecation import MiddlewareMixin
from web import models
from django.conf import settings


class Tartec():
    def __init__(self):
        self.user = None
        self.price_classify = None
        self.project = None


class AuthMiddleware(MiddlewareMixin):
    # 在视图函数执行前调用，如果返回None，则继续处理请求；如果返回HttpResponse对象，则中断后续中间件和视图函数的执行，并直接返回给客户端。
    def process_request(self, request):
        """ 如果用户已登录，则request中赋值 """
        # 实例化对象TarTec 初始化user和price_classify
        request.tartec = Tartec()

        user_id = request.session.get('user_id', 0)
        user_object = models.User.objects.filter(id=user_id).first()
        request.tartec.user = user_object

        # 创建白名单
        """
        1. 获取当前url
        2. 检查当前url是否在白名单中
        """
        # 判断用户是否登录，如果未登录，就重定向到登录页面
        if request.path_info in settings.WHITE_REGEX_URL_LIST:  # 检查当前url 是否在白名单中
            return
        # 不在白名单中
        for url in settings.WHITE_REGEX_PARAMETER_URL_LIST:  # 遍历循环带参数的白名单
            if re.match(url, request.path_info):  # 在看url是否符合发用验证的几个url
                # 不是发送验证的就看是否是登录状态
                return

        if not request.tartec.user:
            return redirect("login")
        # 需要在确认登录之后才能 获取用户额度
        # 获取该用户在交易记录当中最近的一次交易记录
        # _object = models.TransactionRecord.objects.filter(user=user_object, state=1).order_by("-id").first()
        _object = models.TransactionRecord.objects.filter(user=user_object, state=1,
                                                          price_classify=user_object.price_classify).first()
        if _object.end_date and datetime.datetime.now() > _object.end_date:
            # 表示购买的vip 过期
            # 重新赋值给一个免费版的
            # _object = models.TransactionRecord.objects.filter(user=user_object,state=1).order_by("id").first()
            # 两个都可以获取到免费版
            _object = models.TransactionRecord.objects.filter(user=user_object, state=1,
                                                              price_classify__classify=1).first()
            # 保存一下价格策略为免费版
            _object.price_classify = 1
            _object.save()
        # 返回一个用户额度
        request.tartec.price_classify = _object.price_classify

    # 在Django确定要使用的视图函数后但在视图函数实际执行之前调用。同样，如果返回HttpResponse对象，则不再执行视图函数和其他中间件的相应方法。
    def process_view(self, request, view, args, kwargs):
        # 判断URL 是否是以manage 开头并且是自己的项目才能进行访问
        if not request.path_info.startswith("/manage/"):
            return
        # 获取到该用户的所有项目，判断该项目id 是否在其中
        project_id = kwargs.get("project_id")
        project_object = models.Project.objects.filter(created_by=request.tartec.user, id=project_id).first()
        if project_object:
            # 是我创建的项目，让其通过
            request.tartec.project = project_object
            return

        # 是否是我参加的项目
        join_project_object = models.UserProject.objects.filter(user=request.tartec.user, project=project_id).first()
        if join_project_object:
            # 是我参加的项目，让其通过
            request.tartec.project = join_project_object.project
            return

        return redirect("project_list")
