# @Time    : 2024/3/1 22:34
# @Author  : 🍁
# @File    : dashboard.py
# @Software: PyCharm
from django.template import Library

register = Library()


@register.simple_tag
def init_use_space(use_space):
    if use_space > 1024 * 1024 * 1024:
        str_use_space = "%.2f GB" % (use_space / (1024 * 1024 * 1024))
    elif use_space > 1024 * 1024:
        str_use_space = "%.2f MB" % (use_space / (1024 * 1024))
    elif use_space > 1024:
        str_use_space = "%.2f KB" % (use_space / 1024)
    else:
        str_use_space = "%.2f B" % use_space
    return str_use_space
