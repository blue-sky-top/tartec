# @Time    : 2024/2/28 20:27
# @Author  : 🍁
# @File    : issues.py
# @Software: PyCharm
from django.template import Library

register = Library()


@register.simple_tag()
def string_just(num):
    if num < 100:
        # 不满三位的，用0补齐
        num = str(num).rjust(3, "0")
    return "#{}".format(num)
