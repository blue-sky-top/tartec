# @Time    : 2024/1/28 20:50
# @Author  : 🍁
# @File    : project.py
# @Software: PyCharm
from django.template import Library
from django.urls import reverse
from web import models

register = Library()


@register.inclusion_tag('inclusion/all_project_list.html')
def all_project_list(request):
    # 1. 获我创建的所有项目
    my_project_list = models.Project.objects.filter(created_by=request.tartec.user)

    # 2. 获我参与的所有项目
    join_project_list = models.UserProject.objects.filter(user=request.tartec.user)

    return {'my': my_project_list, 'join': join_project_list, 'request': request}


@register.inclusion_tag("inclusion/project_menu_list.html")
def project_menu_list(request):
    data_list = [
        {"title": "概览", "url": reverse("dashboard", kwargs={"project_id": request.tartec.project.id})},
        {"title": "问题", "url": reverse("issues", kwargs={"project_id": request.tartec.project.id})},
        {"title": "统计", "url": reverse("statistic", kwargs={"project_id": request.tartec.project.id})},
        {"title": "wiki", "url": reverse("wiki", kwargs={"project_id": request.tartec.project.id})},
        {"title": "文件", "url": reverse("file", kwargs={"project_id": request.tartec.project.id})},
        {"title": "设置", "url": reverse("setting", kwargs={"project_id": request.tartec.project.id})},
    ]
    for item in data_list:
        # 判断当前访问的url 是否是以上面的url 开头的
        # item["url"] 获取url
        # request.path_info 用户访问的当前url
        if request.path_info.startswith(item["url"]):
            # 添加一个class 默认选中状态
            item["class"] = "active"

    return {"data_list": data_list}
