let app = Vue.createApp({
    delimiters: ['[[', ']]'],
    data() {
        return {
            // v-model
            username: '',   // 用户名
            password: '',   // 密码
            re_password: '',    // 再次输入密码
            email: '',  // 邮箱
            mobile: '', // 手机号
            verify_sms: '', // 短信验证码
            is_choice: false, // 单选框
            send_sms_message: "发送短信验证码",
            send_flag: false, // 用来表示是否发送了验证码

            // v-show
            username_error: false,
            password_error: false,
            re_password_error: false,
            email_error: false,
            mobile_error: false,
            verify_sms_error: false,
            is_choice_error: false,

            //
            username_error_msg: '',
            password_error_msg: '',
            re_password_error_msg: '',
            email_error_msg: '',
            mobile_error_msg: '',
            verify_sms_error_msg: '',
            is_choice_msg: '',
        }
    },
    methods: {
        // 校验用户名
        check_username() {
            let re = /^[a-zA-Z][a-zA-Z0-9_]{4,15}$/
            if (!re.test(this.username)) {
                this.username_error_msg = "用户名不得含有特殊字符"
                this.username_error = true
            } else if (this.username.length < 4) {
                this.username_error_msg = "用户名不得少于4个字符"
                this.username_error = true
            } else {
                this.username_error = false
            }
        },

        // 校验密码
        check_password() {
            let re = /^[a-zA-Z0-9]\w{5,17}$/
            if (!re.test(this.password)) {
                this.password_error_msg = "密码不能以特殊字符开头"
                this.password_error = true
            } else if (this.password.length < 5) {
                this.password_error_msg = "密码不能少于5个字符"
                this.password_error = true
            } else {
                this.password_error = false
            }
        },

        // 校验重复输入密码
        check_re_password() {
            if (this.re_password !== this.password) {
                this.re_password_error_msg = "两次密码不一致"
                this.re_password_error = true
            } else {
                this.re_password_error = false
            }
        },

        // 校验邮箱
        check_email() {
            let re = /^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$/
            if (!re.test(this.email)) {
                this.email_error_msg = "邮箱格式不正确"
                this.email_error = true
            } else {
                this.email_error = false
            }
        },

        // 校验手机号
        check_mobile() {
            let re = /^1[3-9]\d{9}$/
            if (!re.test(this.mobile)) {
                this.mobile_error_msg = "手机号格式不正确"
                this.mobile_error = true
            } else {
                this.mobile_error = false
            }
        },

        // 校验短信验证码
        check_verify_sms() {
            if (this.verify_sms.length !== 6) {
                this.verify_sms_error_msg = "验证码是6位"
                this.verify_sms_error = true
            } else {
                this.verify_sms_error = false
            }
        },

        // 是否点击单选框
        check_is_choice() {
            if (this.is_choice_msg) {
                this.is_choice_msg = "请勾选用户协议"
                this.is_choice_error = true
            } else {
                this.is_choice_error = false
            }
        },

        // 发送验证码
        get_verify_sms() {
            {
                this.check_mobile()
                // 判断是否已经请求发送短信验证码了
                if (this.send_flag === true) {
                    // 验证码已发送 ， 函数停止执行
                    return;
                }
                this.send_flag = true;
                // 请求发送短信验证码的路由
                // sms/mobile/
                let url = "/sms/" + this.mobile + "/";
                axios.get(
                    url,
                    {responseType: 'json'}
                )
                    // 请求成功
                    .then(response => {
                        // 实现60s倒计时
                        if (response.data.code === "0") {
                            console.log(60)
                            let number = 60;
                            // setInterval(回调函数 ， 时间间隔毫秒)
                            let t = setInterval(() => {
                                if (number == 1) {
                                    // 停止回调函数的执行
                                    clearInterval(t);
                                    this.send_sms_message = '获取短信验证码';
                                    // 重新生成一个图片验证码
                                    // 设置允许继续发送请求
                                    this.send_flag = false;
                                } else {
                                    number -= 1;
                                    this.send_sms_message = number + 's';
                                }
                            }, 1000)
                        } else {
                            this.error_sms_code_message = response.data.errsg;
                            this.error_sms_code = true;
                            this.send_flag = false;
                        }
                    })
                    // 请求失败
                    .catch(error => {
                        console.log(error.response)
                        this.send_flag = false;
                    })
            }
        },
        // 注册按钮绑定事件
        on_submit() {
            this.check_email()
            this.check_mobile()
            this.check_password()
            this.check_re_password()
            this.check_username()
            this.check_is_choice()
            if (this.username_error === false && this.password_error === false && this.re_password_error === false && this.mobile_error === false && this.is_choice_error === true) {
                // 禁用点击事件
                window.event.returnValue = false
            }
        }
    }
}).mount('#app')