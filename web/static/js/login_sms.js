let app = Vue.createApp({
    delimiters: ["[[", "]]"],
    data() {
        return {
            // v-model
            mobile: "",
            verify_sms: '', // 短信验证码
            send_sms_message: "发送短信验证码",
            send_flag: false, // 用来表示是否发送了验证码

            // v-show
            mobile_error: false,
            verify_sms_error: false,

            // error_msg
            mobile_error_msg: '',
            verify_sms_error_msg: '',
        }
    },
    methods: {
        // 校验手机号
        check_mobile() {
            let re = /^1[3-9]\d{9}$/
            if (!re.test(this.mobile)) {
                this.mobile_error_msg = "手机号格式不正确"
                this.mobile_error = true
            } else {
                this.mobile_error = false
            }
        },

        // 校验短信验证码
        check_verify_sms() {
            if (this.verify_sms.length !== 6) {
                this.verify_sms_error_msg = "验证码是6位"
                this.verify_sms_error = true
            } else {
                this.verify_sms_error = false
            }
        },
        // 发送验证码
        get_verify_sms() {
            {
                this.check_mobile()
                // 判断是否已经请求发送短信验证码了
                if (this.send_flag === true) {
                    // 验证码已发送 ， 函数停止执行
                    return;
                }
                this.send_flag = true;
                // 请求发送短信验证码的路由
                // ver/sms_code/mobile/?uuid&image_code
                let url = "/sms/" + this.mobile + "/";
                axios.get(
                    url,
                    {responseType: 'json'}
                )
                    // 请求成功
                    .then(response => {
                        // 实现60s倒计时
                        if (response.data.code === "0") {
                            console.log(60)
                            let number = 60;
                            // setInterval(回调函数 ， 时间间隔毫秒)
                            let t = setInterval(() => {
                                if (number == 1) {
                                    // 停止回调函数的执行
                                    clearInterval(t);
                                    this.send_sms_message = '获取短信验证码';
                                    // 重新生成一个图片验证码
                                    // 设置允许继续发送请求
                                    this.send_flag = false;
                                } else {
                                    number -= 1;
                                    this.send_sms_message = number + 's';
                                }
                            }, 1000)
                        } else {
                            this.error_sms_code_message = response.data.errsg;
                            this.error_sms_code = true;
                            this.send_flag = false;
                        }
                    })
                    // 请求失败
                    .catch(error => {
                        console.log(error.response)
                        this.send_flag = false;
                    })
            }
        },
        on_submit() {
            this.check_mobile()
            this.check_verify_sms()
            if (this.mobile_error === true && this.verify_sms_error === true) {
                // 只要有一个错误信息就禁止提交表单
                window.event.returnValue = false
            }
        }
    }
}).mount("#app")