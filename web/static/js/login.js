

const app = Vue.createApp({
    delimiters: ["[[", "]]"],
    data() {
        return {
            // v-model
            username: '',   // 用户名
            password: '',   // 密码
            image_code: "",  //验证码
            uuid: "",
            image_code_url: "", // 图形验证码的url
            is_choice: false, // 单选框


            // v-show
            username_error: false,
            password_error: false,
            image_code_error: false,
            //
            username_error_msg: '',
            password_error_msg: '',
            image_code_error_msg: '',
        }
    },
    // 定义在这个里面的 方法会在页面开始的时候调用一次
    mounted() {
        this.get_image_code()
    },
    methods: {
        // 校验用户名
        check_username() {
            let re = /^[a-zA-Z][a-zA-Z0-9_]{4,15}$/
            if (!re.test(this.username)) {
                this.username_error_msg = "用户名不得含有特殊字符"
                this.username_error = true
            } else if (this.username.length < 4) {
                this.username_error_msg = "用户名不得少于4个字符"
                this.username_error = true
            } else {
                this.username_error = false
            }
        },

        // 校验密码
        check_password() {
            let re = /^[a-zA-Z0-9]\w{5,17}$/
            if (!re.test(this.password)) {
                this.password_error_msg = "密码不能以特殊字符开头"
                this.password_error = true
            } else if (this.password.length < 5) {
                this.password_error_msg = "密码不能少于5个字符"
                this.password_error = true
            } else {
                this.password_error = false
            }
        },
        get_image_code() {
            this.uuid = generateUUID()
            this.image_code_url = "/image/" + this.uuid + "/"
        },
        check_image_code() {
            if (this.image_code.length !== 5) {
                this.image_code_error_msg = "图形验证码是5位"
                this.image_code_error = true
            } else {
                this.image_code_error = false
            }
        },
        check_uuid() {
            let re = /^[\w-]+$/
            if (re.test(this.uuid)){

            }
        },
        on_submit() {
            this.check_username()
            this.check_password()
            if (this.username_error === true && this.password_error === true) {
                window.event.returnValue = false
            }
        },
    }
}).mount("#app")