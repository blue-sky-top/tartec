# @Time    : 2024/1/22 21:45
# @Author  : 🍁
# @File    : project.py
# @Software: PyCharm
from django.shortcuts import render, redirect, reverse
from django.views import View
from django import http
from web.forms.project import ProjectListForm
from web.forms.account import RegisterForms
from web import models
from web.utils.cos.cos import create_bucket
import time


class ProjectListView(View):
    def get(self, request):
        form = ProjectListForm(request)
        # 获取到用户的所有项目并在页面展示出来
        # 从数据库中获取星标项目，创建的项目，参与的项目
        project_dict = {"star": [], "my": [], "join": []}
        # 获取我创建的项目
        all_project = models.Project.objects.filter(created_by=request.tartec.user)
        for row in all_project:  # 遍历我创建的项目
            if row.star:  # 判断是否是星标，如果是星标添加到star 列表里面
                project_dict["star"].append({"value": row, "type": "my"})
            else:  # 如果不是星标添加到my 列表里面
                project_dict["my"].append(row)
        # 获取我参与的项目
        join_project = models.UserProject.objects.filter(user=request.tartec.user)
        for row in join_project:  # 遍历我参与的项目
            if row.star and row.star not in project_dict["star"]:  # 判断是否是星标，并且不在在star 列表
                project_dict["star"].append({"value": row.project, "type": "join"})
            else:
                project_dict["join"].append(row.project)
        return render(request, "project_list.html", {"form": form, "projects": project_dict})

    def post(self, request):
        form = ProjectListForm(request, data=request.POST)
        if form.is_valid():
            # 创建桶
            # {用户的手机号}-{当前时间戳}-1323051912
            bucket = "{0}-{1}-1323051912".format(request.tartec.user.mobile, str(round(time.time() * 1000)))
            region = "ap-beijing"
            create_bucket(bucket=bucket, region=region)
            # 把桶名跟区域赋值
            form.instance.bucket = bucket
            form.instance.region = region
            # 验证通过：项目名，颜色，描述
            # 创建项目
            form.instance.created_by = request.tartec.user
            # 返回一个保存到数据库的对象，也就是保存之后的项目对象
            instance = form.save()
            # 添加问题类型
            issues_project_list = []
            for item in models.IssuesType.PROJECT_INIT_LIST:
                issues_project_list.append(models.IssuesType(project=instance, title=item))
            # 批量添加
            models.IssuesType.objects.bulk_create(issues_project_list)
            return http.JsonResponse({"status": True})
        return http.JsonResponse({"status": False, "error": form.errors})


# 星标项目
class ProjectStarView(View):
    def get(self, request, id, project_type):
        # 按照传过来的project_type 来分类处理星标
        if project_type == "my":
            models.Project.objects.filter(id=id, created_by=request.tartec.user).update(star=True)
            return redirect("project_list")
        if project_type == "join":
            models.UserProject.objects.filter(project_id=id, user=request.tartec.user).update(star=True)
            return redirect("project_list")
        return http.HttpResponse("请求错误")


# 取消星标项目
class UnProjectStarView(View):
    def get(self, request, id, project_type):
        # 按照传过来的project_type 来分类处理星标
        if project_type == "my":
            models.Project.objects.filter(id=id, created_by=request.tartec.user).update(star=False)
            return redirect("project_list")
        if project_type == "join":
            models.UserProject.objects.filter(project_id=id, user=request.tartec.user).update(star=False)
            return redirect("project_list")
        return http.HttpResponse("请求错误")
