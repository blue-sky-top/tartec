# @Time    : 2024/1/18 22:27
# @Author  : 🍁
# @File    : home.py
# @Software: PyCharm
from django.shortcuts import render
from django.views import View


class IndexView(View):
    def get(self, request):
        return render(request, "index.html")
