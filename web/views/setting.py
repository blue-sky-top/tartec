# @Time    : 2024/2/24 20:13
# @Author  : 🍁
# @File    : setting.py
# @Software: PyCharm
from django.shortcuts import render, redirect
from django import http
from django.views import View

from web import models
from web.utils.cos.cos import delete_bucket


# 设置页面
class SettingView(View):
    def get(self, request, project_id):
        return render(request, "setting.html")


# 删除项目
class SettingDeleteView(View):
    def get(self, request, project_id):
        return render(request, "setting_delete.html")

    def post(self, request, project_id):
        project_name = request.POST.get("project_name")
        if not project_name or project_name != request.tartec.project.project_title:
            return render(request, "setting_delete.html", {"error": "项目名错误"})
        models.Project.objects.filter(project_title=project_name)

        # 项目名对就删除（只有项目创建者才能删除）
        if request.tartec.user != request.tartec.project.created_by:
            return render(request, "setting_delete.html", {"error": "您不是项目创建者，不能删除"})

        # 删除桶
        # -- 找到桶中所有文件并删除
        # -- 找到桶中所有碎片并删除
        # -- 删除桶
        delete_bucket(
            bucket=request.tartec.project.bucket,
            region=request.tartec.project.region
        )
        # 删除项目
        models.Project.objects.filter(id=request.tartec.project.id).delete()
        # 删除完成，重定向到项目列表
        return redirect("project_list")
