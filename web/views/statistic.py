# @Time    : 2024/1/28 22:53
# @Author  : 🍁
# @File    : statistic.py
# @Software: PyCharm
from django.views import View
from django.shortcuts import render
from django.db.models import Count
from django import http

from web import models


class StatisticsView(View):
    def get(self, request, project_id):
        return render(request, "statistic.html")


class StatisticsPriorityView(View):
    def get(self, request, project_id):
        start = request.GET.get("start")
        end = request.GET.get("end")

        data_list = []
        data_dict = {}

        for key, text in models.Issues.priority_choices:
            data_dict[key] = {"name": text, "y": 0}

        # 拿到当前项目的按照优先级分组的数据，并统计数量
        priority_data = models.Issues.objects.filter(project_id=project_id, create_datetime__gte=start,
                                                     create_datetime__lt=end).values("priority").annotate(
            ct=Count("id"))
        for item in priority_data:
            data_dict[item["priority"]]["y"] = item["ct"]
        for key in data_dict:
            data_list.append(data_dict[key])
        return http.JsonResponse({"status": True, "data": data_list})


class StatisticsPriorityUserView(View):
    def get(self, request, project_id):
        start = request.GET.get("start")
        end = request.GET.get("end")

        # 创建一个所有项目成员的字典，方便后续的格式化数据
        """
        {
            1:{
                "name":"xxx",
                "status":{
                    1:0,
                    2:0,
                    3:0,
                    4:0,
                    5:0,
                    6:0,
                    7:0,
                }
            }
        }
        """
        all_user_dict = {
            request.tartec.project.created_by_id: {
                "name": request.tartec.project.created_by.username,
                "status": {item[0]: 0 for item in models.Issues.status_choices}
            },
            None: {
                "name": "未指派",
                "status": {item[0]: 0 for item in models.Issues.status_choices}
            }
        }
        # 拿到项目所有成员以及初始化问题信息
        user_list = models.UserProject.objects.filter(project_id=project_id)
        for item in user_list:
            all_user_dict[item.user_id] = {
                "name": item.user_username,
                "status": {item[0]: 0 for item in models.Issues.status_choices}
            }
        issues_list = models.Issues.objects.filter(project_id=project_id, create_datetime__gte=start,
                                                   create_datetime__lt=end)
        for item in issues_list:  # 给项目成员问题信息添加值
            if not item.assign:
                all_user_dict[None]["status"][item.status] += 1
            else:
                all_user_dict[item.assign_id]["status"][item.status] += 1

        # 获取所有项目成员的姓名
        categories = [data["name"] for data in all_user_dict.values()]

        # 给所有的成员的status 赋值
        series_dict = {}
        for key, text in models.Issues.status_choices:
            series_dict[key] = {"name": text, "data": []}

        for key, text in models.Issues.status_choices:
            for item in all_user_dict.values():
                series_dict[key]["data"].append(item["status"][key])

        data = {
            "categories": categories,
            "series": list(series_dict.values())
        }
        return http.JsonResponse({"status": True, "data": data})
