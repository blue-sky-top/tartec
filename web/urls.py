# @Time    : 2023/12/21 10:38
# @Author  : 🍁
# @File    : urls.py
# @Software: PyCharm

from django.urls import path, re_path, include
from web.views import account
from web.views import verify
from web.views import home
from web.views import project
from web.views import statistic
from web.views import wiki
from web.views import file
from web.views import setting
from web.views import issues
from web.views import dashboard
from web.views import price

urlpatterns = [
    # 主页
    path("", home.IndexView.as_view(), name="index"),
    # 注册页面
    path("register/", account.RegisterView.as_view(), name="register"),
    # 发送短信验证码
    re_path(r"^sms/(?P<mobile>1[3-9]\d{9})/$", verify.Send_SMS_view.as_view()),
    # 登录
    path("login/", account.LoginView.as_view(), name="login"),
    # 短信登录
    path("login/sms/", account.LoginSMSView.as_view(), name="login_sms"),
    # 图形验证码
    re_path(r"^image/(?P<uuid>[\w-]+)/$", verify.ImageCodeView.as_view(), name="image_code"),
    # 推出登录
    path("logout/", account.LogoutView.as_view(), name="logout"),
    # 项目管理
    path("project/list/", project.ProjectListView.as_view(), name="project_list"),
    # 星标
    re_path(r"^project/star/(?P<project_type>\w+)/(?P<id>\d+)/$", project.ProjectStarView.as_view(),
            name="project_star"),
    # 取消星标
    re_path(r"^project/unstar/(?P<project_type>\w+)/(?P<id>\d+)/$", project.UnProjectStarView.as_view(),
            name="project_unstar"),

    # 项目管理
    # 使用路由分发的形式
    re_path(r"manage/(?P<project_id>\d+)/", include([
        # 概览
        path("dashboard/", dashboard.DashboardView.as_view(), name="dashboard"),
        path("dashboard/charts/", dashboard.DashboardChartsView.as_view(), name="dashboard_charts"),

        # 问题
        path("issuse/", issues.IssuesView.as_view(), name="issues"),
        # 问题操作页面
        re_path(r"issuse/detail/(?P<issues_id>\d+)/", issues.IssuesDetailView.as_view(), name="issues_detail"),
        # 问题操作记录
        re_path(r"issuse/record/(?P<issues_id>\d+)/", issues.IssuesRecordView.as_view(), name="issues_record"),
        # 操作变更记录
        re_path(r"issuse/change/(?P<issues_id>\d+)/", issues.IssuesChangeRecordView.as_view(), name="issues_change"),
        # 生成邀请码
        path("invite/", issues.InviteView.as_view(), name="invite"),

        # 统计
        path("statistics/", statistic.StatisticsView.as_view(), name="statistic"),
        path("statistics/priority/", statistic.StatisticsPriorityView.as_view(), name="statistics_priority"),
        path("statistics/statistics_project_user/", statistic.StatisticsPriorityUserView.as_view(),
             name="statistics_project_user"),

        # 文件板块
        path("file/", file.FileView.as_view(), name="file"),
        # 文件删除
        path("file/del/", file.DelFile.as_view(), name="del_file"),
        # 前端返回文件 上传到数据库
        path("file/upload/", file.UploadFile.as_view(), name="upload_file"),
        # 获取临时凭证
        path("cos/credential/", file.CosCredential.as_view(), name="cos_credential"),
        # 下载文件
        re_path(r"file/download/(?P<file_id>\d+)/", file.DownloadFileView.as_view(), name="download_file"),

        # wiki板块
        path("wiki/", wiki.WikiView.as_view(), name="wiki"),
        # wiki form
        path("wiki/add/", wiki.AddWikiView.as_view(), name="add_wiki"),
        # wiki 更新
        re_path(r"wiki/update/(?P<update_id>\d+)/", wiki.UpdateWikiView.as_view(), name="update_wiki"),
        # wiki delete
        re_path(r"wiki/delete/(?P<delete_id>\d+)/", wiki.DeleteWikiView.as_view(), name="delete_wiki"),
        # wiki 项目目录
        path("wiki/catalog/", wiki.WikiCatalogView.as_view(), name="wiki_catalog"),
        # wiki 上传图片
        path("wiki/upload/", wiki.WikiUploadView.as_view(), name="wiki_upload"),

        # 设置板块
        path("setting/", setting.SettingView.as_view(), name="setting"),
        # 删除项目
        path("setting/delete/", setting.SettingDeleteView.as_view(), name="delete_project"),
    ])),

    # 邀请码链接
    re_path(r"invite/join/(?P<code>[\w-]+)/", issues.InviteJoin.as_view(), name="invite_join"),

    # 价格
    path("price/", price.PriceView.as_view(), name="price"),
    path("pay/", price.PayView.as_view(), name="pay"),
    path("pay/notify/", price.PayNotifyView.as_view(), name="pay_notify"),
    re_path(r"price/list/(?P<policy_id>\d+)/", price.PriceListView.as_view(), name="payment")
]
