# @Time    : 2024/1/12 21:41
# @Author  : 🍁
# @File    : scrypty.py
# @Software: PyCharm
import bcrypt
import uuid


def hash_password(password):
    """
    加密密码
    :param password:要加密的密码
    :return: 加密过的密码
    """
    # 生成盐值
    salt = bcrypt.gensalt(rounds=12)
    hashed_password = bcrypt.hashpw(password.encode('utf-8'), salt)
    return hashed_password


def check_password(password, hashed_password_from_db):
    """
    校验密码是否正确
    :param password: 用户输入的密码
    :param hashed_password_from_db: 数据库当中的密码
    :return: 校验结果
    """
    if bcrypt.checkpw(password.encode('utf-8'), hashed_password_from_db):
        # 可以继续执行登录逻辑
        return True
    else:
        return False


def uid(string):
    """
    生成一个不会出现重复的值
    :param string: 字符串
    :return: 生成之后的字符串
    """
    data = "{0}-{1}".format(string, str(uuid.uuid4()))
    return data
