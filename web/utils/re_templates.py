# @Time    : 2023/12/12 20:11
# @Author  : 🍁
# @File    : re_templates.py
# @Software: PyCharm

USERNAME = r"^[a-zA-Z][a-zA-Z0-9_]{4,15}$"
PASSWORD = r"^[a-zA-Z0-9]\w{5,17}$"
MOBILE = r"^1[3-9]\d{9}$"
EMAIL = r"^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$"

